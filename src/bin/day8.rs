fn main() {
    let lines: Vec<String> = advent_of_code_2021::parse_input();
    let mut num = 0;
    let mut a = 'a';
    let mut b = 'b';
    let mut c = 'c';
    let mut d = 'd';
    let mut e = 'e';
    let mut f = 'f';
    let mut g = 'g';

    let zero = "abcefg";
    let mut new_zero = "abcefg".to_string();
    let one = "cf";
    let mut new_one = "cf".to_string();
    let two = "acdeg";
    let mut new_two = "acdeg".to_string();
    let three = "acdfg";
    let mut new_three = "acdfg".to_string();
    let four = "bcdf";
    let mut new_four = "bcdf".to_string();
    let five = "abdfg";
    let mut new_five = "abdfg".to_string();
    let six = "abdefg";
    let mut new_six = "abdefg".to_string();
    let seven = "acf";
    let mut new_seven = "acf".to_string();
    let eight = "abcdefg";
    let mut new_eight = "abcdefg".to_string();
    let nine = "abcdfg";
    let mut new_nine = "abcdfg".to_string();

    for line in lines {
        let mut halfs = line.split("|");
        let mut others: Vec<String> = Vec::new();
        for learning in halfs.next().expect("no first half").split(" ") {
            for another_split in learning.split(" ") {
                match another_split.len() {
                    // remember len is in bytes not chars
                    7 => {
                        new_eight = another_split.to_string();
                    }
                    4 => {
                        new_four = another_split.to_string();
                    }
                    3 => {
                        new_seven = another_split.to_string();
                    }
                    2 => {
                        new_one = another_split.to_string();
                    }
                    _ => {
                        others.push(another_split.to_string());
                    }
                }

                //println!("{:?}", another_split);
            }
        }
        // logicly compute all numbers from clues
        a = differences(&new_one, &new_seven)[0];
        for other in others.iter() {
            if other.len() == 5 {
                if differences(&other, &new_one).len() == 3 {
                    new_three = other.clone();
                }
            }
        }
        b = *differences_left(&new_four, &new_three)
            .get(0)
            .expect("Less than 1 element in 4 3 compare");
        for segment in differences_left(&new_three, &new_four) {
            if segment != a {
                g = segment;
            }
        }
        new_nine = format!("{}{}", new_three, b);
        e = *differences_left(&new_eight, &new_nine)
            .get(0)
            .expect("Less than 1 element in 8 9 compare");

        new_zero = format!("{}{}{}{}{}", new_one, a, b, g, e);
        d = *differences_left(&new_eight, &new_zero)
            .get(0)
            .expect("Less than 1 element in 8 0 compare");

        for other in others.iter() {
            if other.len() == 6 {
                if differences(&other, &new_nine).len() != 0
                    && differences(&other, &new_zero).len() != 0
                {
                    new_six = other.clone();
                }
            }
        }
        c = *differences_left(&new_eight, &new_six)
            .get(0)
            .expect("Less than 1 element in 8 6 compare");
        f = *differences_left(&new_eight, &format!("{}{}{}{}{}{}", a, b, c, d, e, g))
            .get(0)
            .expect("Less than 1 element in 8 everthingelse compare");

        new_five = format!("{}{}{}{}{}", a, b, d, f, g);
        new_two = format!("{}{}{}{}{}", a, c, d, e, g);
        // \o/ ok thats all numbers and all leters

        println!(
            "a={}, b={}, c={}, d={}, e={}, f={}, g={}",
            a, b, c, d, e, f, g
        );
        let mut parse_num = 0;
        for output in halfs.next().expect("no second half").split(" ") {
            parse_num *= 10;
            if differences(output, &new_one).len() == 0 {
                parse_num += 1;
            }
            if differences(output, &new_two).len() == 0 {
                parse_num += 2;
            }
            if differences(output, &new_three).len() == 0 {
                parse_num += 3;
            }
            if differences(output, &new_four).len() == 0 {
                parse_num += 4;
            }
            if differences(output, &new_five).len() == 0 {
                parse_num += 5;
            }
            if differences(output, &new_six).len() == 0 {
                parse_num += 6;
            }
            if differences(output, &new_seven).len() == 0 {
                parse_num += 7;
            }
            if differences(output, &new_eight).len() == 0 {
                parse_num += 8;
            }
            if differences(output, &new_nine).len() == 0 {
                parse_num += 9;
            }
            if differences(output, &new_zero).len() == 0 {
                parse_num += 0;
            }
        }
        println!("{}", parse_num);
        num += parse_num;
    }

    print!("{:?}", num);
}

fn differences(left: &str, right: &str) -> Vec<char> {
    let mut result: Vec<char> = Vec::new();
    for left_char in right.chars() {
        let mut matched_char = false;
        'check: for right_char in left.chars() {
            if right_char == left_char {
                matched_char = true;
                break 'check;
            }
        }
        if !matched_char {
            result.push(left_char.clone());
        }
    }
    for left_char in left.chars() {
        let mut matched_char = false;
        'check: for right_char in right.chars() {
            if right_char == left_char {
                matched_char = true;
                break 'check;
            }
        }
        if !matched_char {
            result.push(left_char.clone());
        }
    }
    return result;
}

fn differences_left(left: &str, right: &str) -> Vec<char> {
    let mut result: Vec<char> = Vec::new();
    for left_char in left.chars() {
        let mut matched_char = false;
        'check: for right_char in right.chars() {
            if right_char == left_char {
                matched_char = true;
                break 'check;
            }
        }
        if !matched_char {
            result.push(left_char.clone());
        }
    }
    return result;
}
