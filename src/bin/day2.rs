fn main() {
    let moves: Vec<String> = advent_of_code_2021::parse_input();
    let mut x = 0;
    let mut y = 0;
    let mut aim = 0;
    //let increased = numbers.iter().map(|(x)| ).count();
    for erm in moves {
        let move_split: Vec<&str>  = erm.split(' ').collect();
        if move_split[0].starts_with('f') {
            let dx = move_split[1].parse::<i32>().unwrap();
            x += dx;
            y += (dx*aim);
        }else if move_split[0].starts_with('u') {
            aim -= move_split[1].parse::<i32>().unwrap();
        }else if move_split[0].starts_with('d') {
            aim += move_split[1].parse::<i32>().unwrap();
        }
    }


    //let mut increased: i32 = 0;
    //for i in 3..numbers.len() {
    //    let sum1 = numbers[i - 3] + numbers[i - 2] + numbers[i - 1];
    //    let sum2 = numbers[i - 2] + numbers[i - 1] + numbers[i];
    //    if sum2 > sum1 {
    //        increased += 1;
    //    }
    //}

    print!("{}", x*y);
}