use std::io::{self, BufRead};

fn stuff(l: String) -> Vec<u32> {
   l.chars().map(|c| c.to_digit(10).expect("cantParse into number")).collect()
}
fn both() -> Vec<Vec<u32>> {
    io::stdin()
        .lock()
        .lines()
        .map(|l| stuff(l.expect("could not parse")))
        .collect()

    // let mut my_vec: Vec<i32> = Vec::new();
    // for line in buf.lines() {
    //     let line = line.expect("you fucked up");
    //     my_vec.push(line.parse().expect("Not int"));
    // }
    // return my_vec;
}

fn main() {
    let lines = both();
    let num_lines = lines.len();

    let mut counts: Vec<u32> = vec![0,0,0,0,0,0,0,0,0,0,0,0];
    for line in lines {
        for i in 0..line.len() {
            counts[i] += line[i];

        }
    }
    print!("gamma");
    for count in counts {
        if count > (num_lines / 2).try_into().unwrap() {
                print!("1");

        }else {
            print!("0");
        }

    }



}