fn main() {
    let lines: Vec<Vec<i32>> = advent_of_code_2021::parse_input_split("");

    let mut num = 0;
    for y in 0..lines.len() {
        for x in 0..lines[y].len() {
            if is_lowest_point_4_conected(&lines, x, y, lines[y][x]) {
                println!("{}: y={}, x={}", lines[y][x], y, x);
                num += dbg!(lines[y][x] + 1);
                dbg!(num);
            }
        }
    }
    dbg!(num);
}

fn is_lowest_point_4_conected(map: &[Vec<i32>], x: usize, y: usize, center_height: i32) -> bool {
    if y > 0 {
      match map.get(y - 1) {
          Some(row) => match row.get(x) {
              Some(height) => {
                  if height <= &center_height {
                      return false;
                  }
              }
              None => (),
          },
          None => (),
      }
    }
    match map.get(y) {
        Some(row) => {
            match row.get(x + 1) {
                Some(height) => {
                    if height <= &center_height {
                        return false;
                    }
                }
                None => (),
            }
            if x > 0 {
              match row.get(x - 1) {
                  Some(height) => {
                      if height <= &center_height {
                          return false;
                      }
                  }
                  None => (),
              }
            }
        }
        None => (),
    }

    match map.get(y + 1) {
        Some(row) => match row.get(x) {
            Some(height) => {
                if height <= &center_height {
                    return false;
                }
            }
            None => (),
        },
        None => (),
    }

    true
}
