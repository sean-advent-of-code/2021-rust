use std::io::{self, BufRead};



fn both() -> Vec<i32> {
    let std = io::stdin();
    let mut stdin = std.lock().lines();
    stdin
        .next()
        .expect("Could not read line from standard in")
        .expect("")
        .split(",")
        .map(|s| s.parse().expect("Failed to parse number in first line"))
        .collect()

}

fn save_me(current: Vec<i32>) -> [i64; 9]{
    let mut result = [0,0,0,0,0,0,0,0,0];
    for num in current {
        result[num as usize] += 1;
    }
    result
}

fn main() {
    let mut fish = both();

   let mut fast = dbg!(save_me(fish));

    for i in 0..256 {
        let mut new = [0,0,0,0,0,0,0,0,0];
        new[0] = fast[1];
        new[1] = fast[2];
        new[2] = fast[3];
        new[3] = fast[4];
        new[4] = fast[5];
        new[5] = fast[6];
        new[6] = fast[7] + fast[0];
        new[7] = fast[8];
        new[8] = fast[0];
        fast = new;
    }
     dbg!(fast.iter().sum::<i64>());
}