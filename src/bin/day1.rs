fn main() {
    let numbers: Vec<i32> = advent_of_code_2021::parse_input();

    let mut increased: i32 = 0;
    for i in 3..numbers.len() {
        let sum1 = numbers[i - 3] + numbers[i - 2] + numbers[i - 1];
        let sum2 = numbers[i - 2] + numbers[i - 1] + numbers[i];
        if sum2 > sum1 {
            increased += 1;
        }
    }

    print!("{}", increased);
}
