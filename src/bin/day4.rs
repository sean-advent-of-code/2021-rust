use std::io::{self, BufRead};

#[derive(Debug)]
struct Entry {
    value: i32,
    marked: bool,
}

// todo see what happens if I make a vec of poinnters to transpose the bingo card
type BingoCard = Vec<Vec<Entry>>;

fn stuff(l: String) -> Vec<Entry> {
    l.split_ascii_whitespace()
        .map(|s| Entry {
            value: s.parse().expect("cantParse into number"),
            marked: false,
        })
        .collect()
}
fn both() -> (Vec<i32>, Vec<BingoCard>) {
    let std = io::stdin();
    let mut stdin = std.lock().lines();
    let first_line: Vec<i32> = stdin
        .next()
        .expect("Could not read line from standard in")
        .expect("")
        .split(",")
        .map(|s| s.parse().expect("Failed to parse number in first line"))
        .collect();
    let mut all_bingo_cards: Vec<BingoCard> = Vec::new();

    let mut current_bingo_card: BingoCard = Vec::new();
    for line in stdin {
        let line = line.expect("Could not read line from standard in");
        if line.is_empty() {
            if current_bingo_card.len() > 0 {
                all_bingo_cards.push(current_bingo_card);
                current_bingo_card = Vec::new()
            }
        } else {
            current_bingo_card.push(stuff(line));
        }
    }
    if current_bingo_card.len() > 0 {
        all_bingo_cards.push(current_bingo_card);
    }
    (first_line, all_bingo_cards)
}

fn main() {
    let (moves, mut cards) = both();

    for move_played in moves {
        play_move(move_played, &mut cards);
        //print_boards(&cards);
        let mut winning_card = dbg!(check_winner(&cards));
        while winning_card >= 0 {
            println!("Result is: {}", get_result(&cards, winning_card) * move_played);
            cards.swap_remove(winning_card as usize);
            winning_card = dbg!(check_winner(&cards)); // possible to have multiple winners >:(
        }
    }
}

fn get_result(bingo_cards: &[BingoCard], winning_num: i32) -> i32 {
    let mut sum = 0;
    'cards: for card in bingo_cards.iter().enumerate() {
        if card.0 as i32 != winning_num { continue 'cards; }
        'rows: for row in card.1 {
            'columns: for num in row.iter().enumerate() {
                if !num.1.marked {
                    sum += num.1.value;
                }
            }
        }
    }
    sum
}


fn check_winner(bingo_cards: &[BingoCard]) -> i32 {
    // negative = no winner + is the number of the winning card
    'cards: for card in bingo_cards.iter().enumerate() {
    let mut column_win: Vec<bool> = Vec::new();
        for _num in card.1[0].iter() {
            column_win.push(true);
        }
        'rows: for row in card.1 {
            let mut row_win = true;
            'columns: for num in row.iter().enumerate() {
                if !num.1.marked {
                    row_win = false;
                    column_win[num.0] = false;
                }
            }
            if row_win {
                return card.0 as i32;
            }
        }
        for win in column_win.iter() {
            if *win {
                return card.0 as i32;
            }
        }
    }
    -1
}

fn play_move(move_played: i32, bingo_cards: &mut [BingoCard]) {
    for card in bingo_cards {
        for row in card {
            for num in row {
                if num.value == move_played {
                    num.marked = true;
                }
            }
        }
    }
}

fn print_boards(cards: &[BingoCard]) {
    for card in cards {
        for row in card {
            for num in row {
                if num.marked {
                    print!("({:0>2.0}) ", num.value);
                } else {
                    print!(" {:0>2.0}  ", num.value);
                }
            }
            println!();
        }
        println!();
        println!();
    }
}
