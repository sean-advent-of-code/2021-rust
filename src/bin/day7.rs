use std::io::{self, BufRead};

fn both() -> Vec<i32> {
    let std = io::stdin();
    let mut stdin = std.lock().lines();
    stdin
        .next()
        .expect("Could not read line from standard in")
        .expect("")
        .split(",")
        .map(|s| s.parse().expect("Failed to parse number in first line"))
        .collect()
}

fn main() {
    let mut sorted_values = both();

    sorted_values.sort();
    let mut old_result = i32::MAX;

    for new_num in 0..sorted_values[sorted_values.len() - 1] {
        let mut fuel = 0;
        dbg!(new_num);
        for i in 0..sorted_values.len() {
            let i = i as usize;
            let mut crab_fuel = 0;
            let mut fuel_cost = 0;
            if sorted_values[i] > new_num {
                for _ in new_num..sorted_values[i] {
                    fuel_cost += 1;
                    crab_fuel += fuel_cost;
                }
            } else if sorted_values[i] < new_num {
                for _ in sorted_values[i]..new_num {
                    fuel_cost += 1;
                    crab_fuel += fuel_cost;
                }
            }
            fuel += crab_fuel
        }
        if fuel > old_result {
            break;
        }
        old_result = fuel;
        dbg!(fuel);
    }
    dbg!(old_result);
}
