use std::io::{self, BufRead};

pub fn parse_input<T: std::str::FromStr>() -> Vec<T> {
    io::stdin()
        .lock()
        .lines()
        // using match to prevent T needing debug
        .map(|line| match line.expect("Failed to read line").parse() {
            Ok(val) => val,
            Err(_) => panic!("failed to parse line"),
        })
        .collect()
}
